package com.signumdev.hiltdemoapp.common

interface RecyclerItem {
    fun isSameItem(other: RecyclerItem): Boolean
    fun isSameContent(other: RecyclerItem): Boolean
    fun getType(): Int
    fun getUniqueId(): Any
}