package com.signumdev.hiltdemoapp.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.signumdev.hiltdemoapp.R
import com.signumdev.hiltdemoapp.mvvm.DataStatus
import com.signumdev.hiltdemoapp.viewmodel.WeatherListViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private val viewModel: WeatherListViewModel by viewModels()
    private val adapter = SimpleAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initializeView()
        initializeViewModel()
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        viewModel.getFiveDaysWeather("Lviv")
    }

    private fun initializeView(){
        recyclerView.adapter = adapter
    }

    private fun initializeViewModel(){
        viewModel.dataSource.observe(this, Observer {
            when(it.status){
                DataStatus.SUCCESS -> {
                    it.data?.let { it1 -> adapter.replaceAll(it1.toMutableList()) }
                }
                DataStatus.ERROR -> {
                    Toast.makeText(this, "Error: ${it.error?.message}", Toast.LENGTH_SHORT)
                }
            }
        })
    }
}