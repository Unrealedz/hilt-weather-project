package com.signumdev.hiltdemoapp.ui

import android.content.Context
import android.text.format.DateUtils
import com.signumdev.hiltdemoapp.model.FiveDaysWeatherResponse
import com.signumdev.hiltdemoapp.ui.data.WeatherUiData

class WeatherUiDataMapper(private val context: Context) {

    fun mapUiData(data: FiveDaysWeatherResponse): List<WeatherUiData> {
        return data.weatherDataList.map {
            val date = DateUtils.formatDateTime(
                context,
                it.dt * 1000,
                DateUtils.FORMAT_SHOW_DATE or
                        DateUtils.FORMAT_SHOW_TIME
            )
            val temperature = "${it.main.temp.toInt()} °C"
            val pressure = "${it.main.pressure}"
            val humidity = "${it.main.humidity}"

            WeatherUiData(date, temperature, pressure, humidity)
        }
    }
}