package com.signumdev.hiltdemoapp.viewmodel

import android.content.Context
import androidx.lifecycle.*
import com.signumdev.hiltdemoapp.manager.IWeatherManager
import com.signumdev.hiltdemoapp.mvvm.BaseViewModel
import com.signumdev.hiltdemoapp.mvvm.DataWrapper
import com.signumdev.hiltdemoapp.ui.WeatherUiDataMapper
import com.signumdev.hiltdemoapp.ui.data.WeatherUiData
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WeatherListViewModel @Inject constructor(@ApplicationContext val application: Context,
                                               private val manager: IWeatherManager) : BaseViewModel() {

    private val _dataSource = MutableLiveData<DataWrapper<List<WeatherUiData>>>()
    val dataSource: LiveData<DataWrapper<List<WeatherUiData>>>
        get() = _dataSource

    private val mapper: WeatherUiDataMapper by lazy { WeatherUiDataMapper(application) }

    fun getFiveDaysWeather(city: String) {
        viewModelScope.launch {
            val result = manager.getFiveDayWeather(city)
            val uiData = mapper.mapUiData(result)

            onSuccess(_dataSource, uiData)

        }
    }
}