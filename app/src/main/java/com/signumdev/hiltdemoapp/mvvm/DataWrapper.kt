package com.signumdev.hiltdemoapp.mvvm

data class DataWrapper<T>(val data: T?,
                          val error: Throwable?,
                          @DataStatus val status: Int
)
