package com.signumdev.hiltdemoapp.mvvm;

import androidx.annotation.IntDef;

@IntDef({DataStatus.SUCCESS, DataStatus.LOADING, DataStatus.ERROR})
public @interface DataStatus {
    int SUCCESS = 1;
    int LOADING = 2;
    int ERROR = 3;
}