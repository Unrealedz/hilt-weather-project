package com.signumdev.hiltdemoapp.api

import com.signumdev.hiltdemoapp.Config
import com.signumdev.hiltdemoapp.model.FiveDaysWeatherResponse
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query


interface WeatherService {
    @Headers("Content-Type:application/json; charset=UTF-8")
    @GET("forecast?units=metric")
    suspend fun getFiveDayWeather(
        @Query("appid") apiKey: String = Config.API_KEY,
        @Query("q") cityName: String
    ): FiveDaysWeatherResponse
}