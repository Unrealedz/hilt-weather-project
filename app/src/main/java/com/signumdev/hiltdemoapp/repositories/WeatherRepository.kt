package com.signumdev.hiltdemoapp.repositories

import com.signumdev.hiltdemoapp.api.WeatherService
import com.signumdev.hiltdemoapp.model.FiveDaysWeatherResponse
import javax.inject.Inject

class WeatherRepository @Inject constructor(private val service: WeatherService): IWeatherRepository {
    override suspend fun getFiveDayWeather(cityName: String): FiveDaysWeatherResponse {
        return service.getFiveDayWeather(cityName = cityName)
    }
}

interface IWeatherRepository{
    suspend fun getFiveDayWeather(cityName: String): FiveDaysWeatherResponse
}
