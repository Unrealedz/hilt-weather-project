package com.signumdev.hiltdemoapp.manager

import com.signumdev.hiltdemoapp.model.FiveDaysWeatherResponse

interface IWeatherManager {
    suspend fun getFiveDayWeather(cityName: String): FiveDaysWeatherResponse
}