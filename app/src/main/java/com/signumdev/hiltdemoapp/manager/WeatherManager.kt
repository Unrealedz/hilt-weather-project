package com.signumdev.hiltdemoapp.manager

import com.signumdev.hiltdemoapp.model.FiveDaysWeatherResponse
import com.signumdev.hiltdemoapp.repositories.IWeatherRepository

class WeatherManager(private val weatherRepository: IWeatherRepository): IWeatherManager {

    override suspend fun getFiveDayWeather(cityName: String): FiveDaysWeatherResponse {
       return  weatherRepository.getFiveDayWeather(cityName)
    }

}

