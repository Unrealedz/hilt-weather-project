package com.signumdev.hiltdemoapp.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.signumdev.hiltdemoapp.Config
import com.signumdev.hiltdemoapp.api.WeatherService
import com.signumdev.hiltdemoapp.repositories.IWeatherRepository
import com.signumdev.hiltdemoapp.repositories.WeatherRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.reflect.Modifier
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

private const val CONNECT_TIMEOUT_S = 10L
private const val READ_TIMEOUT_S = 15L

@Module
@InstallIn(SingletonComponent::class)
class ApiModule {

    @Singleton
    @Provides
    fun provideApiUrl(): String  = Config.API_URL

    @Singleton
    @Provides
    fun provideGson(): Gson{
        return GsonBuilder()
            .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
            .serializeNulls()
            .create()
    }

    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient, ApiUrl: String): Retrofit {
        return Retrofit.Builder()
            .baseUrl(ApiUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClient)
            .build()
    }

    @Singleton
    @Provides
    fun provideClient(): OkHttpClient {
        val builder = OkHttpClient.Builder()
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        builder.addInterceptor(logging)

        return builder.build()
    }

    @Singleton
    @Provides
    fun provideApiService(retrofit: Retrofit) =
        retrofit.create(WeatherService::class.java)

}