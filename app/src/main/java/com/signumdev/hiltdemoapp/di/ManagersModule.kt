package com.signumdev.hiltdemoapp.di

import com.signumdev.hiltdemoapp.api.WeatherService
import com.signumdev.hiltdemoapp.manager.IWeatherManager
import com.signumdev.hiltdemoapp.manager.WeatherManager
import com.signumdev.hiltdemoapp.repositories.IWeatherRepository
import com.signumdev.hiltdemoapp.repositories.WeatherRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ManagersModule {

    @Provides
    fun provideWeatherRepository(service: WeatherService): IWeatherRepository =
        WeatherRepository(service)

    @Provides
    fun provideWeatherManager(repository: IWeatherRepository): IWeatherManager{
        return WeatherManager(repository)
    }

}